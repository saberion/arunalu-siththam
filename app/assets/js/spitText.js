/**
 * Minified by jsDelivr using UglifyJS v3.0.24.
 * Original file: /npm/splittext@2.0.0/splitText.js
 * 
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
function displayHelp() {
    console.log("Split text files"), console.log("Version " + version), console.log("");
    for (var t = 0; t < commands.length; t++) {
        for (var e = commands[t], n = "", i = 0; i < e.inputs.length; i++) n += e.inputs[i] + " ";
        n += e.description, console.log(n)
    }
    process.exit()
}

function processArg(t, e) {
    for (var n = 0; n < commands.length; n++) {
        for (var i = commands[n], u = !1, s = 0; s < i.inputs.length; s++)
            if (t == i.inputs[s]) {
                if (null != i.acceptsValue) {
                    if (1 == i.acceptsValue && null == e) return void console.log("Argument " + t + " value is missing!");
                    i.func(e)
                } else i.func(e);
                u = !0
            } 0 == u && (inputFile = t)
    }
}
for (var version = 1, chunks = 2, chunkIndex = 0, inputFile = "", outputPath = "./temp", outputName = "", commands = [{
        description: "Display help.",
        inputs: ["--help", "-h", "/?"],
        func: displayHelp
    }, {
        description: "The input file.",
        inputs: ["--input", "-i", "/i"],
        func: function (t) {
            inputFile = t
        }
    }, {
        description: "The output directory path.",
        inputs: ["--output-dir", "-o", "/o"],
        func: function (t) {
            outputPath = t
        }
    }, {
        description: "Change the output file name.",
        inputs: ["--output-name", "-n", "/n"],
        func: function (t) {
            outputName = t
        }
    }, {
        description: "Number of chunks to split the file into.",
        inputs: ["--chunks", "-c", "/c"],
        acceptsValue: !0,
        func: function (t) {
            1 != isNaN(t) ? chunks = parseInt(t) : console.log("Chunks argument must be an integer.")
        }
    }], i = 0; i < process.argv.length; i++) {
    var arg = process.argv[i],
        nextArg = process.argv[i + 1];
    processArg(arg, nextArg)
}
var fs = require("fs");
if ("" == outputName) {
    var begin = inputFile.lastIndexOf("/"),
        tempName = "";
    begin < 0 && (begin = inputFile.lastIndexOf("\\")), tempName = begin >= 0 ? inputFile.substr(begin + 1) : inputFile, outputName = tempName
}
0 == fs.existsSync(outputPath + "/") && (console.log("mkdir"), fs.mkdirSync(outputPath + "/"));
var currentFile = "",
    currentFileSize = 0,
    totalSize = 0;
console.log("Opening file " + inputFile);
var stats = fs.statSync(inputFile),
    stream = fs.createReadStream(inputFile, {
        autoClose: !0
    });
stream.on("data", function (t) {
    if (currentFile += t.toString(), currentFileSize += t.length, totalSize += t.length, currentFileSize >= parseInt(stats.size / chunks) || totalSize >= parseInt(stats.size)) {
        var e = outputName.lastIndexOf("."),
            n = outputName,
            i = "";
        e > 0 && (n = outputName.substr(0, e), i = outputName.substr(e + 1)), "" != i && (i = "." + i);
        var u = outputPath + "/" + n + chunkIndex + i;
        console.log("Writing to file " + u), fs.writeFileSync(u, currentFile), currentFile = "", currentFileSize = 0, chunkIndex++
    }
});
//# sourceMappingURL=/sm/15d75d55e37c69926e0a3ef1bdcaa992491a2a3ca4478d5812dd42ee4da50d04.map