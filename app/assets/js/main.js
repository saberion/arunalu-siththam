setTimeout(function () {
  $('body').addClass('loaded');
}, 1200);

$(document).ready(function () {

  $('.hamberger').click(function () {
    $('.data > ul').toggleClass("removeul").fadeIn();
  })


  var t4 = new TimelineMax({ paused: true });

  t4.to(".menu", 1, {
    css: {
      width: "100%",
      left: "0%"
    },
    ease: Expo.easeInOut,
    delay: -.5
  });

  t4.staggerFrom(".menu ul li", 1, { x: -200, opacity: 0, autoAlpha: 0, ease: Expo.easeOut }, 0.1);

  t4.reverse();
  $(document).on("click", ".hamberger", function () {

    t4.reversed(!t4.reversed());
  });

  $(document).on("click", ".nav-data", function () {
    t4.reversed(!t4.reversed());
  });




  // end
  var bannerSlider = new Swiper('.swiper-overlay', {
    speed: 3000,
    effect: 'fade',
    autoplay: {
      delay: 5000,
    },
    slidesPerView: 1,
    spaceBetween: 0,
    mousewheel: false,

  });

  var swipercomp = new Swiper('.swiper-container-comp-one', {
    slidesPerView: 3,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var swiperVid = new Swiper('.video-slide', {
    slidesPerView: 2,
    spaceBetween: 30,
    grabCursor: true,
    paginationClickable: false,
    pagination: {
      clickable: false,
    },
    navigation: {
      nextEl: '.play-btn-right',
      prevEl: '.swiper-button-prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      375: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      425: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      680: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      1024: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
    }
  });
  var docSlide = new Swiper('.doc-slide', {
    slidesPerView: 4,
    spaceBetween: 30,
    grabCursor: true,
    pagination: {
      clickable: false,
    },
    navigation: {
      nextEl: '.play-btn-doc-right',
      prevEl: '.swiper-button-doc-prev',
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      425: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      680: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
      1390: {
        slidesPerView: 4,
        spaceBetween: 20,
      },
    }
  });
  var mobileJury = new Swiper('.mobile-jury', {
    speed: 1000,
    effect: 'fade',
    slidesPerView: 2,
    spaceBetween: 0,
    mousewheel: false,
    autoplay: {
      delay: 2500,
      disableOnInteraction: true,
    },
  });

  $('#owl-jury').owlCarousel({
    center: false,
    items: 3,
    margin: 30,
    touchDrag: true,
    nav: true,
    navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
    responsive: {
      990: {
        items: 4,
      },
      680: {
        items: 3,
      },
      425: {
        items: 1,
        margin: 30,
      },
      375: {
        items: 1,
        margin: 30,
      },
      320: {
        items: 1,
        margin: 30,
      }
    }

  });
  var bannerSlider = new Swiper('.competion-one-slide', {
    speed: 1000,
    effect: 'fade',
    autoplay: {
      delay: 1000,
    },
    slidesPerView: 1,
    spaceBetween: 0,
    mousewheel: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

  });


  var menu = ['ELIGIBILITY', 'AWARDS', 'REQUIREMENTS', 'TIMELINE']
  var reqSlide = new Swiper('.req-slide', {
    speed: 1000,
    slidesPerView: 3,
    spaceBetween: 90,
    slidesPerView: 'auto',
    centeredSlides: true,
    loop: true,
    pagination: {
      el: '.menu-slide',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (menu[index]) + '</span>';
      },
    },
    navigation: {
      nextEl: '.req-right',
      prevEl: '.req-left',
    },

  });



  var slider = $(this).find('.image-slider');
  var slides = $(slider).find('.slide');
  var min = 3000;
  var max = 5000;
  var counter = 0;

  if (slides.length > 1) {
    slide(counter, slides);
  }

  function slide(counter, slides) {
    setTimeout(function () {

      if (counter == slides.length - 1) {
        $(slides[counter]).fadeOut(1000);
        counter = -1;
        $(slides[0]).fadeIn(1000);
      } else {
        $(slides[counter]).fadeOut(1000);
        $(slides[counter + 1]).fadeIn(1000);
      }
      counter++;
      slide(counter, slides);
    }, Math.random() * (+max - +min) + +min);
  }



  var sliderT = $(this).find('.compTwoSlider');
  var slidesTW = $(sliderT).find('.slideTW');
  var min = 3000;
  var max = 5000;
  var counter = 0;

  if (slidesTW.length > 1) {
    slide(counter, slidesTW);
  }

  function slide(counter, slidesTW) {
    setTimeout(function () {

      if (counter == slidesTW.length - 1) {
        $(slidesTW[counter]).fadeOut(1000);
        counter = -1;
        $(slidesTW[0]).fadeIn(1000);
      } else {
        $(slidesTW[counter]).fadeOut(1000);
        $(slidesTW[counter + 1]).fadeIn(1000);
      }
      counter++;
      slide(counter, slidesTW);
    }, Math.random() * (+max - +min) + +min);
  }



}) //sdklfjsdfls


// scroll bullet
// a throttle function
function dotsThrottle(func, wait, options) {
  var context, args, result;
  var timeout = null;
  var previous = 0;
  if (!options) options = {};
  var later = function () {
    previous = options.leading === false ? 0 : Date.now();
    timeout = null;
    result = func.apply(context, args);
    if (!timeout) context = args = null;
  };
  return function () {
    var now = Date.now();
    if (!previous && options.leading === false) previous = now;
    var remaining = wait - (now - previous);
    context = this;
    args = arguments;
    if (remaining <= 0 || remaining > wait) {
      if (timeout) {
        clearTimeout(timeout);
        timeout = null;
      }
      previous = now;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }
    return result;
  };
}

// global fixed nav options
let dotFixedNavPresent = false;
let dotFixedNavId = '';
let dotFixedNavUp = false;

// scroll indicator controller
function easyScrollDots(dotfixedOptions) {
  let scrollIndi = document.querySelectorAll('.scroll-indicator');
  dotfixedOptions.fixedNav === true ? dotFixedNavPresent = true : dotFixedNavPresent;
  dotfixedOptions.fixedNavId === '' ? dotFixedNavId = false : dotFixedNavId = dotfixedOptions.fixedNavId;
  dotfixedOptions.fixedNavUpward === true ? dotFixedNavUp = true : dotFixedNavUp;

  if (scrollIndi.length) {
    const scrollIndiTemplate = '<div class="scroll-indicator-controller"><span></span></div>';
    document.querySelector('body').lastElementChild.insertAdjacentHTML('afterend', scrollIndiTemplate);
    const scrollIndiController = document.querySelector('.scroll-indicator-controller');
    if ((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1)) { scrollIndiController.classList.add('indi-mobile'); }
    const scrollIndiElems = Array.prototype.slice.call(scrollIndi);

    scrollIndiElems.forEach(function (e, i) {
      const scrollIndiId = e.getAttribute('id');
      const scrollIndiTitle = e.getAttribute('data-scroll-indicator-title');
      let firstActiveClass = '';

      if (i == 0) {
        firstActiveClass = 'active';
      }
      scrollIndiController.lastElementChild.insertAdjacentHTML('afterend', '<div class="' + firstActiveClass + '" data-indi-controller-id="' + scrollIndiId + '" onclick="scrollIndiClicked(\'' + scrollIndiId + '\');"><span>' + scrollIndiTitle + '</span><div></div></div>');
    });

    const scrollIndiControllerDots = scrollIndiController.querySelectorAll('[data-indi-controller-id]');

    var handleIndiScroll = dotsThrottle(function () {
      let indiScrollTopCollection = {};

      scrollIndiElems.forEach(function (e) {
        const scrollIndiIdScroll = e.getAttribute('id');
        const indiScrollTop = e.getBoundingClientRect().top;

        indiScrollTopCollection[scrollIndiIdScroll] = indiScrollTop;
      });

      // const indiOffsetValues = Object.values(indiScrollTopCollection); not supported in ie
      const indiOffsetValues = Object.keys(indiScrollTopCollection).map(function (itm) { return indiScrollTopCollection[itm]; });
      const indiOffsetMin = function () {
        const indiRemoveMinuses = indiOffsetValues.filter(function (x) { return x > -150; });

        return Math.min.apply(null, indiRemoveMinuses);
      };

      Object.keys(indiScrollTopCollection).forEach(function (e) {
        if (indiScrollTopCollection[e] == indiOffsetMin()) {
          Array.prototype.forEach.call(scrollIndiControllerDots, function (el) {
            if (el.classList.contains('active')) {
              el.classList.remove('active');
            }
          });
          scrollIndiController.querySelector('[data-indi-controller-id="' + e + '"]').classList.add('active');
        }
      });

    }, 300);

    window.onscroll = function () {
      handleIndiScroll();
    };
  }
}

function scrollIndiClicked(indiId) {
  if (window.jQuery) {
    // if jquery is availble then we can use jquery animations
    if (dotFixedNavPresent === true && dotFixedNavId.length) {
      // there is a fixed nav and its id has been defined
      const dotNavHeightElem = document.getElementById(dotFixedNavId);
      const dotNavHeight = dotNavHeightElem.clientHeight;
      const dotDocumentHtml = $('html, body');
      const indiElement = $('#' + indiId);

      if (dotFixedNavUp === true) {
        // fix nav on upward scroll only
        dotDocumentHtml.animate({
          scrollTop: indiElement.offset().top
        }, 700);
        const scrollPos = document.body.getBoundingClientRect().top;
        setTimeout(function () {
          if (document.body.getBoundingClientRect().top > scrollPos) {
            dotDocumentHtml.animate({
              scrollTop: indiElement.offset().top - dotNavHeight
            }, 400);
          }
        }, 400);
      }
      else {
        // fixed nav scroll
        dotDocumentHtml.animate({
          scrollTop: indiElement.offset().top - dotNavHeight
        }, 700);
      }
    }
    else {
      // normal scroll
      $('html, body').animate({
        scrollTop: $('#' + indiId).offset().top
      }, 700);
    }
  }
  else {
    // there is no jquery so we use vanilla scroll animations
    if (dotFixedNavPresent === true && dotFixedNavId.length) {
      // there is a fixed nav and its id has been defined
      const dotNavHeightElem = document.getElementById(dotFixedNavId);
      const dotNavHeight = dotNavHeightElem.clientHeight;
      const indiElement = document.getElementById(indiId);

      if (dotFixedNavUp === true) {
        // fix nav on upward scroll only
        window.scrollTo({
          top: indiElement.offsetTop,
          left: 0,
          behavior: 'smooth'
        });
        const scrollPos = document.body.getBoundingClientRect().top;
        setTimeout(function () {
          if (document.body.getBoundingClientRect().top > scrollPos) {
            window.scrollTo({
              top: indiElement.offsetTop - dotNavHeight,
              left: 0,
              behavior: 'smooth'
            });
          }
        }, 400);
      }
      else {
        // fixed nav scroll
        window.scrollTo({
          top: indiElement.offsetTop - dotNavHeight,
          left: 0,
          behavior: 'smooth'
        });
      }
    }
    else {
      // normal scroll
      window.scrollTo({
        top: document.getElementById(indiId).offsetTop,
        left: 0,
        behavior: 'smooth'
      });
    }
  }
}

// Init
easyScrollDots({
  'fixedNav': false,
  'fixedNavId': '',
  'fixedNavUpward': false
});




// scroll bullet end

//scroll animation

//scroll animation end

// wait until DOM is ready
document.addEventListener("DOMContentLoaded", function (event) {

  console.log("js dom loaded");
  // wait until window is loaded - all images, styles-sheets, fonts, links, and other media assets
  // you could also use addEventListener() instead
  window.onload = function () {


    // OPTIONAL - waits til next tick render to run code (prevents running in the middle of render tick)
    window.requestAnimationFrame(function () {

      var controller = new ScrollMagic.Controller();

      var Tl = new TimelineMax({ pasued: true });
      var Tr = new TimelineMax({ pasued: true });

      // banner elements 
      var banner = $('.main-banner');
      var LMenu = $('.left-menu');
      var RMenu = $('.right-menu');
      var logo = $(LMenu).find('.logo');
      var pcc = $(LMenu).find('.pcc-sec');
      var hamberger = $(RMenu).find('.hamberger');
      var chatMo = $(RMenu).find('.text-ani-inner');
      var diomand = $('.scroll-indicator-controller');
      var social_btn = $(LMenu).find('.text-ani-inner');
      var bannerText = $(banner).find(".banner-text-section .text-ani-inner");
      var banneBox = $(banner).find(".box");
      var bg_slider = $(banner).find(".mask-slider");
      // competion 
      var compTimeLine = new TimelineMax({ pasued: true });
      var info = $('.competition-home');
      var infoImg = $(info).find('.competition-home-left');
      var infoDis = $(info).find('.competition-home-discript');
      // details
      var DetailTimeline = new TimelineMax({ pasued: true });
      var Details = $('.wrap-detals-section');

      var animationSpeed = 0.5;
      var animationSpeedTwo = 1;
      var animationTimeIn = Expo.easeIn;
      var animationTimeOut = Expo.easeOut;
      // jury section 
      var JurySection = new TimelineMax({ pasued: true });

      Tl.fromTo(
        logo,
        animationSpeed,
        { y: 50, opacity: 0, ease: animationTimeIn },
        { y: 0, opacity: 1, ease: animationTimeOut },
      )
        .fromTo(
          pcc,
          animationSpeed,
          { y: 50, opacity: 0, ease: animationTimeIn },
          { y: 0, opacity: 1, ease: animationTimeOut },
        )
        .staggerFromTo(
          social_btn,
          animationSpeed,
          { y: 50, opacity: 0, ease: animationTimeIn },
          { y: 0, opacity: 1, ease: animationTimeOut },
          0.15
        )
        .staggerFromTo(
          bannerText,
          animationSpeed,
          { y: '50%', opacity: 0, ease: animationTimeIn },
          { y: '0%', opacity: 1, ease: animationTimeOut },
          0.15
        ).fromTo(
          bg_slider,
          animationSpeed,
          { opacity: 0, ease: animationTimeIn },
          { opacity: 1, ease: animationTimeOut },
          "-=1"
        )
        .staggerFromTo(
          banneBox,
          animationSpeed,
          { y: '50%', opacity: 0, ease: animationTimeIn },
          { y: '0%', opacity: 1, ease: animationTimeOut },
          0.15
        );
      Tl.delay(0.2);
      Tl.play();


      Tr.fromTo(
        hamberger,
        animationSpeed,
        { y: 50, opacity: 0, ease: animationTimeIn },
        { y: 0, opacity: 1, ease: animationTimeOut },
      )
        .fromTo(
          diomand,
          animationSpeed,
          { y: 50, opacity: 0, ease: animationTimeIn },
          { y: 0, opacity: 1, ease: animationTimeOut },
        )
        .staggerFromTo(
          chatMo,
          animationSpeed,
          { y: '120%', opacity: 0, ease: animationTimeIn },
          { y: '0%', opacity: 1, ease: animationTimeOut },
          0.15
        );
      Tr.delay(0.2);
      Tr.play();

      // competiotion
      compTimeLine.from(
        infoImg,
        animationSpeed,
        { y: 100, ease: animationTimeIn, opacity: 0 },
        { y: 0, ease: animationTimeOut, opacity: 1 },
      )
        .staggerFromTo(
          infoDis,
          animationSpeed,
          { y: 100, ease: animationTimeIn, opacity: 0 },
          { y: 0, ease: animationTimeOut, opacity: 1 },
        )

      // competiontion 
      new ScrollMagic.Scene({
        triggerElement: '#home-competition',
        triggerHook: 0.1,
      }).setTween(compTimeLine)
        .addTo(controller);

      // detaisl slider 

      DetailTimeline.from(
        Details,
        animationSpeed,
        { ease: animationTimeIn, opacity: 0 },
        { ease: animationTimeOut, opacity: 1 },
      )

      new ScrollMagic.Scene({
        triggerElement: '.wrap-detals-section',
        triggerHook: 0.1,
      }).setTween(DetailTimeline)
        .addTo(controller);

      // jury section 

      JurySection.from(
        '.port-text',
        animationSpeed,
        { duration: 0.5, autoAlpha: 0, stagger: 1 / 10 },
      )
        .from(
          '.box1-slide',
          3,
          { y: 40, autoAlpha: 0, },
          '-=4')
        .from(
          '.box2-slide',
          3,
          { y: 45, autoAlpha: 0, },
          '-=3.5')
        .from(
          '.box3-slide',
          3,
          { y: 50, autoAlpha: 0, },
          '-=3.5')
        .from(
          '.box4-slide',
          3,
          { y: 55, autoAlpha: 0, },
          '-=3.5')
        .from(
          '.box5-slide',
          3,
          { y: 60, autoAlpha: 0, },
          '-=3.5')


      new ScrollMagic.Scene({
        triggerElement: '#home-jury',
        duration: '20%',
        triggerHook: 0.4,
      }).setTween(JurySection)
        .addTo(controller)


      $(".img-sec").each(function () {
        var Iml = new TimelineMax();
        var cov = $(this).find(".white-panel");
        var img = $(this).find(".pContent");

        Iml.from(cov, 1, { scaleX: 0, transformOrigin: "left" });
        Iml.to(cov, 1, { scaleX: 0, transformOrigin: "right" }, "reveal");
        Iml.from(img, 1, { opacity: 0 }, "reveal");

        var scene = new ScrollMagic.Scene({
          triggerElement: this,
          triggerHook: 0.5
        })
          .setTween(Iml)
          .addTo(controller);
      });





    });

  };

});


